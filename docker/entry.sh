#!/bin/sh
set -e
dockerize -wait tcp://postgres:5432
exec "$@"
